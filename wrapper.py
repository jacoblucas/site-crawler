from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

chromedriver = '/usr/local/bin/chromedriver'
driver = Chrome(chromedriver)

def get_num_pages():
    '''
    get number of pages
    '''
    driver.get('https://shop.nordstrom.com/c/womens-shoes?origin=topnav&breadcrumb=Home%2fWomen%2fShoes')
    try:
        page_nums = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME, "_18xGX"))
        )
    except:
        print('except')

    pages = []
    for i in page_nums:
        pages.append(i.text)
    num_pages = int(pages[-1])

    # driver.close()

    return num_pages

def get_shoe_links_page(page_no):
    '''
    Scrapes main page to get link to all shoes on page
    '''
    link = 'https://shop.nordstrom.com/c/womens-shoes?origin=topnav&breadcrumb=Home%2FWomen%2FShoes&page={}'.format(page_no)
    driver.get(link)
    try:
        elem = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located((By.TAG_NAME, "a"))
        )
    except:
        print('except')
    ls = []
    for i in elem:
        link = i.get_attribute('href')
        ls.append(link)

    return ls

def get_prices(driver,link):
    '''
    Get price for specific shoe on page and return dictionary with prices
    Some shoes are on sale which is reason for 2 prices
    '''
    # link = 'https://shop.nordstrom.com/s/birkenstock-essentials-arizona-slide-sandal-women/4152948/full?origin=category-personalizedsort&breadcrumb=Home%2FWomen%2FShoes&color=metallic%20silver'
    driver = Chrome(chromedriver)
    driver.get(link)
    try:
        now = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, "_1ds4c"))
            )
    except:
        now = 0
    try:
        was = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, "_2bkjG"))
            )
    except:
        was = 0

    d = {}
    if now == 0:
        d['now'] = 0
    else:
        for i in now:
            d['now'] = i.text
    if was == 0:
        d['was'] = 0
    else:
        for i in was:
            d['was'] = i.text
    driver.close()

    return d

def get_product_no(link):
    '''
    Get product no in link
    '''
    link = 'https://shop.nordstrom.com/s/birkenstock-essentials-arizona-slide-sandal-women/4152948/full?origin=category-personalizedsort&breadcrumb=Home%2FWomen%2FShoes&color=metallic%20silver'
    driver.get(link)
    prod_no_link = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.TAG_NAME, 'link'))
            )

    for i in prod_no_link:
        prod_no = 0
        link = i.get_attribute('href')
        if link[-7:].isdigit():
            prod_no = link[-7:]
        else:
            pass

    return prod_no

def get_product_description(link):
    '''
    Get product description
    '''
    link = 'https://shop.nordstrom.com/s/birkenstock-essentials-arizona-slide-sandal-women/4152948/full?origin=category-personalizedsort&breadcrumb=Home%2FWomen%2FShoes&color=metallic%20silver'
    driver.get(link)
    details = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, 'ErtZY'))
            )
    ls = []
    for i in details:
        ls.append(i.text)

    driver.close()

    return ls
