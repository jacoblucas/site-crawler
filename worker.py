from wrapper import get_num_pages, get_shoe_links_page
from selenium.webdriver import Chrome
import pandas as pd

chromedriver = '/usr/local/bin/chromedriver'
driver = Chrome(chromedriver)

def get_all_shoe_links():
    no_pages = get_num_pages()
    for i in range(1,no_pages+1):
        df = pd.read_csv('links.csv')
        link_ls = get_shoe_links_page(i)
        df2 = pd.DataFrame({'links':link_ls})
        df = df.append(df2, ignore_index=True)
        df.to_csv('links.csv',index=False)

get_all_shoe_links()
